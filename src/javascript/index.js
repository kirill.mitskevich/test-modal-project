window.TestModal = (function() {
  var modalsCache = {};
  var $body = document.querySelector('body');

  addEventListeners();

  return TestModal;

  function TestModal(element) {
    if (typeof element === 'string') {
      return getOrCreateModal(element, document.querySelector(element));
    } else {
      return getOrCreateModal(element.id, element);
    }
  }

  function getOrCreateModal(elementId, element) {
    if (modalsCache[elementId]) {
      return modalsCache[elementId];
    }

    var modal = createModal(element);
    modalsCache[elementId] = modal;

    return modal;
  }

  function createModal(element) {
    var modalState = {
      element: element
    };

    return {
      open: openModal.bind(modalState),
      close: closeModal.bind(modalState),
      toggle: toggleModal.bind(modalState)
    };
  }

  function addEventListeners() {
    document.querySelector('body').addEventListener('click', function(event) {
      var target = event.target;
      var modalId = target.dataset.modalId;

      if (target.classList.contains('js-open-modal')) {
        new TestModal(modalId).open();
      } else if (target.classList.contains('js-close-modal')) {
        new TestModal(modalId).close();
      } else if (target.classList.contains('js-toggle-modal')) {
        new TestModal(modalId).toggle();
      }
    });
  }

  function openModal() {
    this.element.classList.add('modal--shown');
    $body.classList.add('--modal-shown');
  }

  function closeModal() {
    this.element.classList.remove('modal--shown');
    $body.classList.remove('--modal-shown');
  }

  function toggleModal() {
    if (this.element.classList.contains('modal--shown')) {
      closeModal.call(this);
    } else {
      openModal.call(this);
    }
  }
})();
